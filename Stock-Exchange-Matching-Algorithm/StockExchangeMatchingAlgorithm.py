#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'computePrices' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts following parameters:
#  1. INTEGER_ARRAY s
#  2. INTEGER_ARRAY p
#  3. INTEGER_ARRAY q
#

def computePrices(s, p, q):
    val=[]
    prices=[]
    maximo=0
    for m in q:
        for S in s:
            if(m>=S):
                val.append(S)
        if(len(val)>=1):
            maximo=max(val)
            for a, b in zip(s, p):
                if(maximo==a):
                    prices.append(b)
        val.clear()
    return prices

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    n = int(input().strip())
    if(n>=1 and n<=10**5):
        s = list(map(int, input().rstrip().split()))
        pas=True
        valS=[]
        uniq=True
        uniqU=False
        prueba=0
        for i in s:
            if(i>=1 and i<=(10**9)):
                valS.append(i)
                pas=True
            else:
                pas=False
            if(pas==False):
                break
            prueba=valS.count(i)
            if(prueba>=2):
                uniq=False
                if(uniq==False):
                    break
        if(valS.count(1)):
            uniqU=True
        if(pas==True and uniq==True and uniqU==True):
            p = list(map(int, input().rstrip().split()))
            for x in p:
                if(x>=1 and x<=(10**9)):
                    pas=True
                else:
                    pas=False
                if(pas==False):
                    break
            if(pas==True):
                k = int(input().strip())
                if(k>=1 and k<=10**5):
                    q = []
                    for _ in range(k):
                        q_item = int(input().strip())
                        q.append(q_item)
                    for y in q:
                        if(y>=1 and y<=(10**9)):
                            pas=True
                        else:
                            pas=False
                        if(pas==False):
                            break
                    res = computePrices(s, p, q)
                    fptr.write('\n'.join(map(str, res)))
                    fptr.write('\n')
    fptr.close()
