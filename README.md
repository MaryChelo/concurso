# Concurso

Concurso de programación [Goldman Sachs Women's CodeSprint](https://www.hackerrank.com/goldman-sachs-womens-codesprint) de hackerrank.com 

```
El objetivo de este repositorio es mostrar la evidencia de participación en el concurso de programación.
```

## Fechas
Este concurso se realizó del 5 de octubre del 2018 de las 9 am al 7 de octubre del 2018 a las 9 am.

## Qué hacen en Goldman Sachs
En Goldman Sachs, nuestros equipos de ingeniería de software construyen soluciones para algunos de los proyectos más complejos de la industria. Desde el comercio automatizado hasta la gestión de datos, el análisis de riesgos y la protección de la información y la promoción de la responsabilidad ambiental, nuestro compromiso con la mejor tecnología de su clase proporciona a Goldman Sachs una ventaja competitiva.

## Reglas del concurso
* El concurso se llevará a cabo desde el viernes 5 de octubre a las 9 am PDT hasta el domingo 7 de octubre a las 9 am PDT.
* Debe identificarse como mujer para ser elegible para ganar un premio.

* Este es un concurso de participación individual.

* Por favor, absténgase de discutir la estrategia durante la competición.

* Cualquier caso de plagio de código resultará en descalificación. Todas las presentaciones son revisadas por un detector de plagio.

* Codifique directamente desde nuestra plataforma, que soporta más de 30 idiomas. 

* Las decisiones de Goldman Sachs y HackerRank son finales.

* Los premios se otorgarán a los participantes que acepten compartir su información con Goldman Sachs.

* Los empleados actuales de Goldman Sachs no son elegibles para recibir premios.

* Por favor, espere 6-8 semanas después de la competencia para recibir premios.

* Los participantes se clasifican por puntuación. 

## Desafíos a resolver

**Stock Exchange Matching Algorithm** <br/>  
> Success Rate: 51.39% Max Score: 20 Difficulty: Easy
_Objetivo: Imprimir el precio de una acción, que dará el algoritmo._

**Elevator Travel** <br/>  
> Success Rate: 39.21% Max Score: 30 Difficulty: Medium
_Objetivo: Calcular la distancia mínima de viaje entre pisos de un elevador que puede lograr al hacerlo._

**Party Invitations** <br/>   
> Success Rate: 50.33% Max Score: 40 Difficulty: Medium
_Objetivo: Imprimir el número de formas de enviar invitaciones a todos los empleados._

**Stringonomics** <br/>   
> Success Rate: 24.42% Max Score: 60 Difficulty: Hard
_Objetivo: Encontrar el número mínimo de consultas que se ejecutan secuencialmente de la existencia de una subcadena en una cadena._

**Decimal Array Expansion** <br/>   
> Success Rate: 21.05% Max Score: 80 Difficulty: Hard.
_Objetivo expandir una secuencia de tamaño A a una secuencia de tamaño B._

## Evidencia de participación
**Lenguaje**
> Python 3

**Rank Final**
![](RankFinal.png) <br/>
**Rank Usuario**
![](userRank.png) <br/>

**Stock Exchange Matching Algorithm**
> Puntuación obtenida: 10

<br/>

```python
#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'computePrices' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts following parameters:
#  1. INTEGER_ARRAY s
#  2. INTEGER_ARRAY p
#  3. INTEGER_ARRAY q
#

def computePrices(s, p, q):
    val=[]
    prices=[]
    maximo=0
    for m in q:
        for S in s:
            if(m>=S):
                val.append(S)
        if(len(val)>=1):
            maximo=max(val)
            for a, b in zip(s, p):
                if(maximo==a):
                    prices.append(b)
        val.clear()
    return prices

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    n = int(input().strip())
    if(n>=1 and n<=10**5):
        s = list(map(int, input().rstrip().split()))
        pas=True
        valS=[]
        uniq=True
        uniqU=False
        prueba=0
        for i in s:
            if(i>=1 and i<=(10**9)):
                valS.append(i)
                pas=True
            else:
                pas=False
            if(pas==False):
                break
            prueba=valS.count(i)
            if(prueba>=2):
                uniq=False
                if(uniq==False):
                    break
        if(valS.count(1)):
            uniqU=True
        if(pas==True and uniq==True and uniqU==True):
            p = list(map(int, input().rstrip().split()))
            for x in p:
                if(x>=1 and x<=(10**9)):
                    pas=True
                else:
                    pas=False
                if(pas==False):
                    break
            if(pas==True):
                k = int(input().strip())
                if(k>=1 and k<=10**5):
                    q = []
                    for _ in range(k):
                        q_item = int(input().strip())
                        q.append(q_item)
                    for y in q:
                        if(y>=1 and y<=(10**9)):
                            pas=True
                        else:
                            pas=False
                        if(pas==False):
                            break
                    res = computePrices(s, p, q)
                    fptr.write('\n'.join(map(str, res)))
                    fptr.write('\n')
    fptr.close()
```

<br/>

![](Stock-Exchange-Matching-Algorithm/Problema1.4_Resultados.png) <br/>

<br/>

**Elevator Travel**

> Puntuación obtenida: 2.4

```python

#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'solve' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY p as parameter.
def solve(p):
    listP = len(p)
    suma=0
    for i in range(0, listP-1):
        if(p[i]!=p[i+1]):
            pas=True
        else:
            pas=False
            break
    if(pas==True):
        for i in range(0, listP-1):
            if(p[i]<p[i+1]):
                suma+= abs(p[i]-p[i+1])
    return suma
    
if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    p_count = int(input().strip())
    if(p_count>=2 and p_count<=5000):
        p = list(map(int, input().rstrip().split()))
        if(p.count(0)==0 and len(p)==p_count):
            p.insert(0, 0)
            cont=0
            pas=False
            for i in p:
                if(i>=0 and i<=(p_count)):
                    pas=True
                else:
                    pas=False
                    break
            if(pas==True):
                result = solve(p)
            fptr.write(str(result) + '\n')
    fptr.close()
```

<br/>

![](Elevator-Travel/Problema2.3_Resultado.png) 

<br/>

![](Elevator-Travel/Problema2.4_Resultado.png)

<br/>

**Stringonomics**

> Puntuación obtenida: 0

```python
#!/bin/python3

import math
import os
import random
import re
import sys

if __name__ == '__main__':
    pas=False
    t = int(input().strip())
    if(t>=1 and t<=50 ):

        for t_itr in range(t):
            s = input()
            if(len(s)>=1 and len(s)<=(2*10**5)):
                p = input()
                if(len(p)>=1 and len(p)<=len(s)):
                    q = int(input().strip())
                    if(q>=1 and q<=len(s)):
                        for q_itr in range(q):
                            first_multiple_input = input().rstrip().split()
                            x = int(first_multiple_input[0])
                            if(x>=0 and x<len(s)):
                                c = first_multiple_input[1][0]
                        c=c.lower()
                        s=s.lower()
                        lisS=[]
                        lisS.append(s)
                        lisSu=list(s)
                        separador=""
                        lisS=[separador.join(lisSu)]
                        if(p in lisS):
                            for n, i in enumerate(listS):
                                if (n==x):
                                    listS[n]=c
                                    print(2)
                                else:
                                    print(-1)

```

**Enviados**
![](Submission.png)

## Referencias
* https://www.hackerrank.com/contests/goldman-sachs-womens-codesprint/challenges