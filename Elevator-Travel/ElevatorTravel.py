#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'solve' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY p as parameter.
#

def solve(p):
    listP = len(p)
    suma=0
    for i in range(0, listP-1):
        if(p[i]!=p[i+1]):
            pas=True
        else:
            pas=False
            break
    if(pas==True):
        for i in range(0, listP-1):
            if(p[i]<p[i+1]):
                suma+= abs(p[i]-p[i+1])
    return suma
    

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    p_count = int(input().strip())
    if(p_count>=2 and p_count<=5000):
        p = list(map(int, input().rstrip().split()))
        if(p.count(0)==0 and len(p)==p_count):
            p.insert(0, 0)
            cont=0
            pas=False
            for i in p:
                if(i>=0 and i<=(p_count)):
                    pas=True
                else:
                    pas=False
                    break
            if(pas==True):
                result = solve(p)
            fptr.write(str(result) + '\n')
    fptr.close()
